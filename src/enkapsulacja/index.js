var widget1 = document.querySelector("#tutaj-wrzuc-mi-widget1");
var widget2 = document.querySelector("#tutaj-wrzuc-mi-widget2");
var widget3 = document.querySelector("#tutaj-wrzuc-mi-widget3");

function Widget(elem, buttonSelector) {
  this.elem = elem;
  if (buttonSelector) {
    // Override prototype (shared) selector
    this._buttonSelector = buttonSelector;
  }

  this._elemButton = elem.querySelector(this._buttonSelector);

  this._elemButton.addEventListener(
    "click",
    function() {
      this.checkAndToogle(this.elem);
    }.bind(this)
  );
}
// Overridable in instance
Widget.prototype._buttonSelector = "button";

// Override Object prototype
Widget.prototype.toString = function(){ return '[Widget]' }

Widget.prototype.checkAndToogle = function() {
  if (this.elem.classList.contains("active")) {
    this.closePanel();
  } else {
    this.openPanel();
  }
};

Widget.prototype.closePanel = function() {
  this.elem.classList.remove("active");
};

Widget.prototype.openPanel = function() {
  this.elem.classList.add("active");
};

// Start!

var w1 = new Widget(widget1);

var w2 = new Widget(widget2,'.toggle');
w2.openPanel();

var w3 = new Widget(widget3);

// ==

// document.addEventListener('placki', bind(WhatIsThis, otherThis ))

// function bind(fn, that) {
//   return function(arg) {
//     that.fn(arg);
//   };
// }
