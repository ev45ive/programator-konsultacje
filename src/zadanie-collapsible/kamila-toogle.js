function MakeCollapsible(collapsedSelector, toggleSelector) {
  var collapsible = document.querySelector(collapsedSelector),
    toggle = document.querySelector(toggleSelector);
    
  toggle.addEventListener("click", function(e) {
    collapsible.classList.toggle("show");
    toggle.textContent = collapsible.classList.contains("show")
      ? "Toggle"
      : "Pokaż ponownie";
  });
}

// ===

MakeCollapsible(".collapse", "button.toggle");
