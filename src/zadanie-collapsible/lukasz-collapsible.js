var collapsible = document.querySelector(".collapse");
var toggle = document.querySelector("button.toggle");

function MakeCollapsible(collapsibleElem, collapsibleButton) {
 collapsibleButton.addEventListener("click", function() {
   collapsibleElem.classList.toggle("show");
 });
}

MakeCollapsible(collapsible, toggle);

// if (collapsibleElem.style.display === "none") {
//   collapsibleElem.style.display = "block";
// } else {
//   collapsibleElem.style.display = "none";
// }

// var showContent = false;
//   collapsibleButton.addEventListener("click", function() {
//     if (!showContent) {
//       collapsibleElem.classList.add("show");
//       showContent = true;
//     } else {
//       collapsibleElem.classList.remove("show");
//       showContent = false;
//     }
//   });