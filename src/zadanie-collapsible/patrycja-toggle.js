function MakeCollapsible(collapsibleElem, collapsibleButton) {
  collapsibleButton.onclick = function() {
    var classList = collapsibleElem.classList;

    if (classList.contains("show")) {
      classList.remove("show");
      collapsibleButton.innerText = "Pokaż ten tekst o ciasteczkach";
    } else {
      classList.add("show");
      collapsibleButton.innerText = "Tak, rozumiem!";
    }
  };
}

var collapsible = document.querySelector(".collapse");
var toggle = document.querySelector("button.toggle");

MakeCollapsible(collapsible, toggle);
