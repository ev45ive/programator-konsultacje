function MakeCollapsible(collapsibleSelector, toggleSelector, options) {
  var defaults = {
    className: "show",
    ActiveButtonText: "Zamknij",
    InactiveBUttonText: "Pokaż ponownie"
  };

  options = Object.assign({}, defaults, options);

  var collapsibleElem = document.querySelector(collapsibleSelector);
  var collapsibleButton = document.querySelector(toggleSelector);
  
  // ===

  collapsibleButton.addEventListener("click", function() {
    var isVisible = collapsibleElem.classList.contains(options.className);

    collapsibleElem.classList.toggle(options.className, !isVisible);

    collapsibleButton.textContent = isVisible
      ? options.InactiveBUttonText
      : options.ActiveButtonText;
  });
}

// <div class="collapse" id="collapseExample">
//   <div class="card card-body">
//     Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica
//   </div>
// </div>
// <button class="toggle">Toggle</button>

MakeCollapsible("#collapseExample", "[data-collapse=collapseExample]");
MakeCollapsible("#collapseExample2", "[data-collapse=collapseExample2]", {
  ActiveButtonText: "Schowaj"
});
