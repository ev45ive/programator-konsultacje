// REST API
/* 
  RepresEntational
  State
  Transfer

  Application
  Programming
  Interface

  Transfer
  Reprezentacji
  Stanu

  Resource

  /kolekcja (np GET /products?query=13 POST /products {})
  /kolekcja/123 (np. GET /products/123 PUT /products/123 DELETE products/123 )
  
  GET /users/123/cart
  POST /users/123/cart/ { product_id:456, amount:123 }
  PUT /users/123/cart/ { product_id:456, amount:123 }

  // VS

  RPC
  /addProductToUserCart?user_id=345&product_id=45&amount=2


  Remote Procedure Call


*/

function ProductsAPI(baseUrl) {
  this.baseUrl = baseUrl;
}

// Pobierz produkty ( /products lub /products?page=1 pierwsze 10)
ProductsAPI.prototype.findAll = function(callback) {
  sendGET(this.baseUrl, callback);
};

// Wyszukaj produkty po nazwie ( http://naszserver.com/products/?q=placki )
ProductsAPI.prototype.findByName = function(name, callback) {
  sendGET(this.baseUrl + "?q=" + name, callback);
};

// Pobierz produkt po ID ( http://naszserver.com/products/123 )
ProductsAPI.prototype.findById = function(id, callback) {
  sendGET(this.baseUrl + "/" + id, callback);
};

// Dodaj produkt createOne({name:'placki',price:123})
ProductsAPI.prototype.createOne = function(data, callback) {
  sendPOST(this.baseUrl, data, callback);
};

// Zaktualizuj dane produktu  updateOne({id:123, name:'placki',price:123})
ProductsAPI.prototype.updateOne = function(data, callback) {
  sendPUT(this.baseUrl + "/" + data.id, data, callback);
};

// Usuń Produkt
ProductsAPI.prototype.deleteOne = function(id, callback) {
  sendDELETE(this.baseUrl + "/" + id, callback);
};

function AJAX() {}

function sendGET(url, callback) {
  sendRequest("GET", url, null, callback);
}

function sendPOST(url, data, callback) {
  sendRequest("POST", url, data, callback);
}

function sendPUT(url, data, callback) {
  sendRequest("PUT", url, data, callback);
}

function sendPATCH(url, data, callback) {
  sendRequest("PATCH", url, data, callback);
}

function sendDELETE(url, callback) {
  sendRequest("DELETE", url, null, callback);
}

function sendRequest(method, url, data, callback) {
  var xhr = new XMLHttpRequest();
  xhr.onload = function() {
    callback(JSON.parse(xhr.response));
  };
  // xhr.onerror
  xhr.open(method, url);
  if (data) {
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(JSON.stringify(data));
  } else {
    xhr.send();
  }
}
