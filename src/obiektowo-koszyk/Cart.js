function Cart(selector) {
  ItemsList.call(this, selector);
}

Cart.prototype = Object.create(ItemsList.prototype);

Cart.prototype._renderItem = function(cartItem) {
  var cartItemDiv = document.createElement("div");
  cartItemDiv.innerHTML = cartItem.product.name + " " + cartItem.product.price; // + suma
  return cartItemDiv;
};

Cart.prototype.add = function(product) {
  this._items.push({
    product: product,
    amount: 1
  });
  this.render();
};
