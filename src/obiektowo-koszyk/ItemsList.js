function ItemsList(selector) {
  this._items = []; // Array<???>
  this.selector = selector;
}

// ProductsList.prototype = Object.create({});

ItemsList.prototype._renderItem = function() {
  throw "Not Implemented";
};

ItemsList.prototype.render = function() {
  cartListElem = document.querySelector(this.selector);
  cartListElem.innerHTML = "";
  var divs = this._items.map(this._renderItem);
  divs.forEach(function(cartItemDiv) {
    cartListElem.append(cartItemDiv);
  });
};

// ===  ES6

/* 
class ItemsList {

  _renderItem() {
    throw "Not Implemented";
  }

  render() {
    this._renderItem();
  }
}

*/