
function ProductsList(selector, products) {
  ItemsList.call(this, selector);

  if (products) {
    this.loadProducts(products);
  }
}

ProductsList.prototype = Object.create(ItemsList.prototype);

ProductsList.prototype._renderItem = function(product) {
  var productDIv = document.createElement("div");
  productDIv.innerHTML = product.name + " " + product.price;
  return productDIv;
};

ProductsList.prototype.loadProducts = function(products) {
  this._items = products;
  this.render();
};

// === ES6
 
/* 

class ProductsList extends ItemsList{

  // render() from ItemsList
  
  _renderItem(){

  }
} 

*/