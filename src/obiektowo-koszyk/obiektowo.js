function makePerson(name){
    
    var person = {
        sayHello: function(){ return 'I am '+name }
    }
    return person 
}
​
var alice = makePerson('Alice')

// ===
var obj = { name:'Jestem obiektem' }

function WhatIsThis(){
	console.log(this) 
}

WhatIsThis() //Window
window.WhatIsThis() // Window

obj.what = WhatIsThis
obj.what() // { name:'Jestem obiektem' }

// ==
var obj = { name:'Jestem obiektem' }

function WhatIsTheName(){
	console.log(this.name)
}

WhatIsTheName() // window.name // ''
window.name = 'Placki'
WhatIsTheName() // Placki
window.WhatIsTheName() // Placki

obj.WhatIsYourName = WhatIsTheName
obj.WhatIsYourName() //Jestem obiektem

// ===

function makePerson(name){
	this.name = name;
	this.sayHello =  function(){ return 'I am '+ this.name }

	return this 
}
makePerson('Windows 3.11')
window.name // "Windows 3.11"
sayHello() // "I am Windows 3.11"

// ===

function makePerson(name){
	this.name = name;
	this.sayHello =  function(){ return 'I am '+ this.name }

	return this 
}

var Pinoccio = {
	makePerson: makePerson
}
Pinoccio.makePerson('Pinoccio')
Pinoccio.sayHello() // "I am Pinoccio"

// ===
var alice = {}
makePerson.call( alice, 'Alice')
// alice.__proto__.constructor // ƒ Object() { [native code] }

// ===
var bob = new makePerson('Bob')
// bob.__proto__.constructor // ƒ makePerson(name){

// ===

function Person(name){
	this.name = name;
	this.sayHello =  function(){ return 'I am '+ this.name }
}

var kate = new Person('Catherine')
kate.sayHello() //"I am Catherine"

kate.__proto__.constructor // ƒ Person(name){

kate instanceof Person
true

// Type of objects
jabko instanceof Product 
ananas instanceof Product
alice instanceof Person 

// === 

function makePerson(name,age){

	var person =  {
		name: name, 
		age:age,
		sayHello: function(){ return 'Jestem ' + person.name },
		tellMeAboutYou: function(){ return  this.sayHello() + ' and I am '+ person.age },
    }

	return person;
}

alice = makePerson('Alice', 21);

// {name: "Alice", age: 21, sayHello: ƒ, tellMeAboutYou: ƒ}
alice.tellMeAboutYou()
"Jestem Alice and I am 21"

// ==
function makePerson(name,age){

	this.name = name
	this.age= age

	this.sayHello = function(){ return 'Jestem ' + this.name }
	this.tellMeAboutYou = function(){ return  this.sayHello() + ' and I am '+ this.age }
	
}
var alice = new makePerson('Alice',21);
alice.tellMeAboutYou()
"Jestem Alice and I am 21"
 
// == 

function makePerson(name,age){

	this.name = name
	this.age= age

	this.sayHello = sayHello
	this.tellMeAboutYou = tellMeAboutYou
	
}
function sayHello (){ return 'Jestem ' + this.name }
function tellMeAboutYou(){ return  this.sayHello() + ' and I am '+ this.age }

var alice = new makePerson('Alice',21);
console.log(alice.tellMeAboutYou())

var bob = new makePerson('Bob',41);
bob.tellMeAboutYou()


// VM3225:14 Jestem Alice and I am 21
"Jestem Bob and I am 41"
window.sayHello()
"Jestem "
window.tellMeAboutYou()
"Jestem  and I am undefined"

// ==

function Person(name,age){

	this.name = name
	this.age= age
}

Person.prototype.sayHello = function (){ return 'Jestem ' + this.name }
Person.prototype.tellMeAboutYou =  function (){ return  this.sayHello() + ' and I am '+ this.age }

// var alice = {__proto__: Person.prototype}
var alice = Object.create(Person.prototype)
// alice.Person = Person; alice.Person('Alice',21);
Person.call(alice,'Alice',32);

// var alice = new Person('Alice',21);
// console.log(alice.tellMeAboutYou())

var bob = new Person('Bob',41);
// bob.tellMeAboutYou()

alice.__proto__.constructor == Person
true

alice instanceof Person
true

bob.__proto__.constructor == Person
true

bob instanceof Person
true

// ==


var alice = new Person('Alice',21);
var bob = new Person('Bob',31);

alice.sayHello()
"Jestem Alice"
alice.sayHello = function(){ return 'I wont give you my telephone or even my name' }

alice.tellMeAboutYou()
"I wont give you my telephone or even my name and I am 21"

bob.tellMeAboutYou()
"Jestem Bob and I am 31"

// ==

obj = {a:1, b:2}
// {a: 1, b: 2}
obj2 = { c: 3}
// {c: 3}
Object.assign(obj, obj2)
// {a: 1, b: 2, c: 3}
obj 
// {a: 1, b: 2, c: 3}
obj2
// {c: 3}
Object.assign({},obj,obj2,{placki:'gratis!'})
// {a: 1, b: 2, c: 3, placki: "gratis!"}