var Api = new ProductsAPI("http://localhost:3000/products");

function ProductsList(el) {
  this.$el = $(el);
  this.$tbody = this.$el.find("tbody");
  this.updateProductslist();
  this.bindProductEvents();
}

ProductsList.prototype.onSelect = function(){ throw 'onSelect - Method Not Implemented' }

ProductsList.prototype.selectProduct = function(id) {
  var trs = this.$tbody.find("tr");
  trs.removeClass("table-active");

  trs.filter("tr[data-product-id=" + id + "]").addClass("table-active");
};
ProductsList.prototype.updateProductslist = function() {
  Api.findAll(
    function(products) {
      // var tbody = $("tbody",this.$el);
      // var tbody = this.$el.find("tbody");
      this.$tbody.html("");

      products.forEach(
        function(product) {
          var tr = $(
            "<tr>" +
              "<td>" +
              product.name +
              "</td>" +
              "<td>" +
              product.price +
              "</td>" +
              "<td></td>" +
              "</tr>"
          );
          // tr.data("product-id", product.id);
          tr.attr("data-product-id", product.id);
          this.$tbody.append(tr);
        }.bind(this)
      );
    }.bind(this)
  );
};

ProductsList.prototype.bindProductEvents = function() {
  this.$tbody.on(
    "click",
    "tr",
    function(event) {
      var tr = $(event.currentTarget);
      var id = tr.data("product-id");
      this.onSelect(id);
    }.bind(this)
  );
};

function ProductDetails(el) {
  this.$el = $(el);
  this.$el.text("Prosze wybrac produkt");
  this.$el.on("click", ".product-close-btn-js", function() {
    this.onSelect(null);
  });
}

ProductDetails.prototype.showProductDetails = function(product_id) {
  if (product_id == null) {
    this.$el.text("Prosze wybrac produkt");
    return;
  }
  this.$el.html("<div>Ładowanie ...</div>");

  $.getJSON("http://localhost:3000/products/" + product_id) //
    .done(
      function(product) {
        this.$el.empty();
        this.$el.append(
          $(
            "<div>" +
              "<dl>" +
              "<dt>Nazwa</dt>" +
              "<dd>" +
              product.name +
              "</dd>" +
              "<dt>Cena</dt>" +
              "<dd>" +
              product.price +
              "</dd>" +
              "</dl>" +
              '<button class="btn btn-danger product-close-btn-js">Zamknij</button>' +
              "</div>"
          )
        );
      }.bind(this)
    );
};

var list = new ProductsList(".products-js");
var details = new ProductDetails(".product-details-js");

list.onSelect = function(id) {
  details.showProductDetails(id);
};

details.onSelect = function(id) {
  list.selectProduct(id);
};

// ===

var list2 = new ProductsList($(".products2-js"));
var details2 = new ProductDetails(".product-details2-js");

// list2.onSelect = function(id) {
//   details2.showProductDetails(id);
// };

details2.onSelect = function(id) {
  list2.selectProduct(id);
};

// $("tbody tr").removeClass("table-active");
// if (id) {
//   var tr = $("tbody tr[data-product-id=" + id + "]");
//   tr.addClass("table-active");
//   showProductDetails(id);
// } else {
//   productDiv.empty();
//   productDiv.text("Prosze wybrac produkt");
// }
