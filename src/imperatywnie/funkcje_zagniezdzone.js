function A() {
  B();
}
function B() {
  C();
}
function C() {
  console.log("C");
}

debugger;
A();

// ===

function A() {
  return B();
}

function B() {
  var osoba = C();
  return osoba;
}

function C() {
  return "Catherin";
}

debugger;
console.log(A());

// ===
var osoby = ["Kasia", "Patrycja", "Zenon"];

function A(powitanie) {
  return powitanie + ", " + B();
}

function B() {
  var osoba = C();
  return osoba;
}

function C() {
  var losowa = Math.floor(Math.random() * 3);
  return osoby[losowa];
}

// debugger;
console.log(A("No Hej"));

// ===

function test() {
  var marchewka = 123;

  function wewnatrz() {
    console.log(marchewka);
  }

  wewnatrz();
}
test();

// ===
// Wybucha!!! :O
function rekurencja() {
  rekurencja();
}

debugger;
rekurencja();
// ===
function rekurencjaA() {
  rekurencjaB();
}

function rekurencjaB() {
  if (Math.random() > 0.5) {
    rekurencjaA();
  }
}

rekurencjaA();
// ===

function fibonacci(num) {
  if (num <= 1) return 1;

  return fibonacci(num - 1) + fibonacci(num - 2);
}
//===
// Shallow copy
function clone(obj) {
  var copy = {};
  for (var key in obj) {
    copy[key] = obj[key];
  }
  return copy;
}
o1 = { a: 1, b: 2, c: { x: 123 } };
o2 = clone(o1);
// ===
// Deep copy
function clone(obj) {
  var copy = {};
  for (var key in obj) {
    if (typeof obj[key] == "object") {
      copy[key] = clone(obj[key]);
    } else {
      copy[key] = obj[key];
    }
  }
  return copy;
}
o1 = { a: 1, b: 2, c: { x: 123 } };
o2 = clone(o1);
// ==
o1.boom = o1;
clone(o1); // Uncaught RangeError: Maximum call stack size exceeded
// ==
var lista = [1, 2, 3, 4, 5];
lista.forEach(function(x, i, all) {
  console.log(x, i, all);
});

// ==
lista.sort(function(x, y) {
  return x - y;
});
lista.sort(function(x, y) {
  return y - x;
});
// ==
lista.map(function(x) {});
// (5) [undefined, undefined, undefined, undefined, undefined]
lista.map(function(x) {
  return "placki";
});
// (5) ["placki", "placki", "placki", "placki", "placki"]
lista.map(function(x) {
  return x * 2;
});
// (5) [10, 8, 6, 4, 2]
lista;
// (5) [5, 4, 3, 2, 1]
// ==
lista.filter(function(x) {
  return true;
});
// (5) [5, 4, 3, 2, 1]
lista.filter(function(x) {
  return false;
});
// []
lista.filter(function(x) {
  return x > 3;
});
// (2) [5, 4]
lista.filter(function(x) {
  return x % 2 == 0;
});
// (2) [4, 2]
// ==
var suma = 0;
lista.forEach(function(x) {
  suma += x;
});
suma;
// ==

var suma = 0;
lista.forEach(function(x) {
  suma += x;
});
suma;
// 15

lista.reduce(function(sum, next) {
  return (sum += next);
}, 0);
// 15
// ==

lista.reduce(
  function(group, next) {
    group[next % 2 == 0 ? "even" : "odd"] += next;
    return group;
  },
  {
    odd: 0,
    even: 0
  }
);
// ==
lista.reduce(
  function(group, next) {
    var key = next % 2 == 0 ? "even" : "odd";
    group[key].push(next);
    return group;
  },
  {
    odd: [],
    even: []
  }
);
// {
//   odd: Array(3), even: Array(2)}
//   even: (2) [2, 4]
// }
