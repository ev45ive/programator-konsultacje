console.log("Koszyk!");

var products = [
  {
    id: 1,
    name: "Placki",
    price: 95.9,
    tax: 1.23,
    discount: 0.05
  },
  {
    id: 2,
    name: "Marchewka",
    price: 20.0,
    tax: 1.23,
    discount: 0
  },
  {
    id: 3,
    name: "Jabłka",
    price: 10.0,
    tax: 1.23,
    discount: 0
  }
];

var cart_items = [
  {
    amount: 1,
    product: products[0]
  },
  {
    amount: 2,
    product: products[1]
  }
];

// Lista produktów
var productsListElem = document.querySelector(".products-list");
renderProducts(productsListElem, products, renderProduct);

// Lista w koszyku
var cartListItems = document.querySelector(".cart-items-list");
renderProducts(cartListItems, cart_items, renderCartItem);

// Suma
updateTotal();

function renderProducts(listElement, listItems, renderFunction) {
  for (var i in listItems) {
    var product = listItems[i];

    var productElem = renderFunction(product);

    listElement.append(productElem);
  }
}

function renderProduct(product) {
  var productDiv = document.createElement("div");
  productDiv.id = "product_" + product.id;
  productDiv.dataset.productId = product.id;

  productDiv.classList.add("list-group-item");
  var price_gross = product.price * product.tax * (1 - product.discount);

  productDiv.innerHTML =
    product.name + (product.discount > 0 ? " <b>PROMOTION</b> " : "");
  productDiv.innerHTML +=
    '<div><div><button class="float-right">Add</button></div></div>';

  // var addToCartBtn = productDiv.querySelector("button");
  // addToCartBtn.addEventListener("click", function(event) {
  //   console.log(
  //     "add to cart",
  //     product,
  //     event.target.closest("[data-product-id]").dataset.productId
  //   );
  // });

  // debugger;

  return productDiv;
}

function renderCartItem(cart_item) {
  var cartItemElem = document.createElement("div");
  cartItemElem.dataset["cartProductId"] = cart_item.product.id;
  cartItemElem.classList.add("list-group-item");
  cartItemElem.innerHTML =
    cart_item.product.name +
    " x " +
    cart_item.amount +
    '<button class="float-right add_to_cart">+</button>' +
    '<button class="float-right remove_form_cart">-</button>';

  return cartItemElem;
}

productsListElem.addEventListener("click", function(e) {
  if (e.target.matches("button")) {
    var id = e.target.closest("[data-product-id]").dataset["productId"];
    addToCart(id);
  }
});

cartListItems.addEventListener("click", function(e) {
  var productId = e.target.closest("[data-cart-product-id]").dataset[
    "cartProductId"
  ];
  if (e.target.matches(".add_to_cart")) {
    addToCart(productId);
  } else if (e.target.matches(".remove_form_cart")) {
    removeFromCart(productId);
  }
});

function removeFromCart(product_id) {
  var cart_item = findCartItemByProductId(product_id);
  if (cart_item) {
    cart_item.amount -= 1;
    var cartItemDiv = cartListItems.querySelector(
      "[data-cart-product-id='" + cart_item.product.id + "']"
    );
    var newItem = renderCartItem(cart_item);
    cartListItems.insertBefore(newItem, cartItemDiv);
    cartItemDiv.remove();

    if (cart_item.amount <= 0) {
      newItem.remove();
      cart_items.splice(cart_items.indexOf(cart_item), 1);
    }
  }
  updateTotal();
}

function addToCart(id) {
  var product = findProductById(id);
  var cart_item = findCartItemByProductId(product.id);

  if (cart_item) {
    cart_item.amount += 1;
    var cartItemDiv = cartListItems.querySelector(
      "[data-cart-product-id='" + cart_item.product.id + "']"
    );
    var newItem = renderCartItem(cart_item);
    cartListItems.insertBefore(newItem, cartItemDiv);
    cartItemDiv.remove();
  } else {
    cart_item = {
      product: product,
      amount: 1
    };
    cart_items.push(cart_item);
    var cartItemElem = renderCartItem(cart_item);
    cartListItems.appendChild(cartItemElem);
  }
  updateTotal();
  // cartListItems.innerHTML = "";
  // renderProducts(cartListItems, cart_items, renderCartItem);
}

function updateTotal() {
  var total = cart_items.reduce(function(total, cart_item) {
    return (total += cart_item.product.price * cart_item.amount);
  }, 0);
  document.querySelector(".cart-total").innerHTML = total.toFixed(2);
}

function findCartItemByProductId(product_id) {
  return cart_items
    .filter(function(cart_item) {
      return cart_item.product.id == product_id;
    })
    .pop();
}

function findProductById(id) {
  var product = null;
  for (var index in products) {
    var currentProduct = products[index];
    if (currentProduct.id == id) {
      product = currentProduct;
      break;
    }
  }
  return product;
}
