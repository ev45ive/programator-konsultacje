// Abstract Class:
function AbstractTool(){}
  AbstractTool.prototype.start = function(){ throw 'Not Implemented' }
  AbstractTool.prototype.moveTo = function(){ throw 'Not Implemented' }
  AbstractTool.prototype.cancel = function(){ throw 'Not Implemented' }
  AbstractTool.prototype.end = function(){ throw 'Not Implemented' }

// Concrete Polymorphic Implementations
function MoveTool(){}
  MoveTool.prototype = Object.create(AbstractTool.prototype)
  Object.assign(MoveTool.prototype,{
      start: function(event){ 
        this.active = event.target; 
        var x = event.pageX, y = event.pageY, target = this.active;
        
        target.style.left = x +'px'
        target.style.top = y +'px'

        console.log('selected object') },
      moveTo: function(event){ {
        var x = event.pageX, y = event.pageY, target = this.active;
        if(!target){ return }
        target.style.left = x +'px'
        target.style.top = y +'px'
        
        console.log('move object') 
      } },
      cancel: function(){  },
      end: function(){ this.active = false; console.log('drop object') }
  })
  
function DrawLineTool(){}
  DrawLineTool.prototype = Object.create(AbstractTool.prototype)
  DrawLineTool.prototype.start = function(){ this.active = true; console.log('create line start') }
  DrawLineTool.prototype.moveTo = function(){  this.active && console.log('update line end') }
  DrawLineTool.prototype.cancel = function(){  }
  DrawLineTool.prototype.end = function(){ this.active = false; }

function DrawFreehandTool(){}
  DrawFreehandTool.prototype = Object.create(AbstractTool.prototype)
  Object.assign(DrawFreehandTool.prototype,{
    start: function(){ this.active = true;  console.log('draw point') },
    moveTo: function(){ this.active &&  console.log('draw point') },
    cancel: function(){   },
    end: function(){ this.active = false; }
  })

document.addEventListener('mousedown', function(event){ tool.start(event) });
document.addEventListener('mousemove',  function(event){ tool.moveTo(event) });
document.addEventListener('mouseup', function(event){ tool.end(event) })


tool = new MoveTool()