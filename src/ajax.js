function downloadJSON(url, readData) {
  var xhr = new XMLHttpRequest();

  // xhr.onreadystatechange = function(e) {
  //   console.log(e.type, e.target.readyState);
  // };
  // xhr.onload = function(e) {
  //   console.log(e.type, e.target.readyState);
  // };
  // xhr.onerror = function(e) {
  //   console.log(e.type, e.target.readyState);
  // };
  
  xhr.onloadend = function(e) {
    readData(JSON.parse(xhr.response), xhr);
  };

  xhr.readyState; // 0
  xhr.open("GET", url);
  xhr.readyState; // 1
  xhr.send();

  return xhr;
}

// downloadJSON('products.json', function(response){
//   console.log(response);
// })
